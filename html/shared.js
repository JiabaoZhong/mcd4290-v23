// before starting, Please read the Requirements in the GG


// Class 
class Trip {
    constructor (tripName,startTime,createdTime,finishTime,route,country,totalDistance) {
        this._tripName = tripName;
        this._startTime = startTime;
        this._createdTime = createdTime;
        this._finishTime = finishTime;
        this._route = route;
        this._country = country;
        this._totalDistance = totalDistance;
    }
    getTripName () {
        return this._tripName
    }
    getStartTime () {
        return this._startTime
    }
    getCreatedTime () {
        return this._createdTime
    }
    getFinishTime () {
        return this._finishTime
    }
    getRoute () {
        return this._route
    }
    getCountry () {
        return this._country
    }
    getTotalDistance () {
        return this._totalDistance
    }
    getSourceAirport () {
        return this._route[0]
    }
    getDistinationAirport () {
        return this._route[this._route.length-1]
    }
    setTripName (tripname) {
        this._tripName = tripname
    }
    setStartTime (starttime) {
        this._startTime = starttime
    }
    setCreatedTime (createdtime) {
        this._createdTime = createdtime
    }
    setFinishTime (finishtime) {
        this._finishTime = finishtime
    }
    setRoute (route) {
        this._route = route
    }
    setCountry (country) {
        this._country = country
    }
    setTotalDistance (totaldistance) {
        this._totalDistance = totaldistance
    }
    addAirport (airport) {
        this._route.push(airport)
    }
    fromData (dataObject) {
        this._tripName = dataObject._tripName
        this._startTime = dataObject._startTime
        this._createdTime = dataObject._createdTime
        this._finishTime = dataObject._finishTime
        this._route = dataObject._route
        this._country = dataObject._country
        this._totalDistance = dataObject._totalDistance
    }
}
    
    
    }
}






































//functions



















//page load